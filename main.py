import argparse

from functools import singledispatch

from bs4 import BeautifulSoup


@singledispatch
def cmp(val1, val2):
    return val1 == val2


@cmp.register(list)
def _(val1, val2):
    result = 0
    for value in val1:
        result += value in val2
    return result


class Tag:
    def __init__(self, bs_tag=None):
        self.name = bs_tag.name
        self.text = bs_tag.text
        self._attrs = bs_tag.attrs
        self.path = self.xpath_soup(bs_tag)

    @property
    def attrs(self):
        return self._attrs

    def compare(self, tag):
        result = 0
        result += self.name == tag.name
        result += self.text == tag.text
        for key, value in self.attrs.items():
            if key in tag.attrs:
                result += cmp(value, tag.attrs[key])
        return result

    def xpath_soup(self, element):
        components = []
        child = element if element.name else element.parent
        for parent in child.parents:
            siblings = parent.find_all(child.name, recursive=False)
            position = next(i for i, s in enumerate(siblings, 1) if s is child)
            components.append(
                child.name if len(siblings) == 1 else
                f'{child.name}[{position}]'
            )
            child = parent
        return f'{">".join(reversed(components))}'


class TagFinder:
    """
    :param document: html document
    :param original: html document with target
    :param taret_id: target id
    """
    def __init__(self, document: str, original: str, target_id: str):
        self.document_path = document
        self.original_path = original
        self.target_id = target_id
        self.original = None
        self.similar_tags = None
        self.load_original_element()
        self.find_similar_tags()

    def load_original_element(self):
        with open(self.original_path) as file:
            soup = BeautifulSoup(file, 'html.parser')
            result = soup.find(id=self.target_id)
            self.original = Tag(bs_tag=result)

    def find_similar_tags(self):
        """
        Finds similar tags.

        Tag is similar to another if at least one attribute matches
        """
        with open(self.document_path) as file:
            soup = BeautifulSoup(file, 'html.parser')
            tags = set()
            for attr, value in self.original.attrs.items():
                tags.update(soup.find_all(attrs={attr: value}))
            self.similar_tags = {Tag(tag) for tag in tags}

    def most_similar(self):
        res = {}
        for tag in self.similar_tags:
            res[self.original.compare(tag)] = tag.path
        return res[max(res.keys())]


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('original',
                        help='origin sample path to find the '
                             'element with attribute '
                             'id="make-everything-ok-button" and '
                             'collect all the required '
                             'information')
    parser.add_argument('sample', help='path to diff-case HTML file '
                                                'to search a similar element')
    parser.add_argument('target_id', nargs='?',
                        help='make-everything-ok-button')
    args = parser.parse_args()
    finder = TagFinder(document=args.sample,
                       original=args.original,
                       target_id=args.target_id or 'make-everything-ok-button')
    print(finder.most_similar())
