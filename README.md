HTML Most Similar Element
========================

To begin you should have the following applications installed on your
local development system:

- Python >= 3.7
- pip <http://www.pip-installer.org/> >= 18.0
- virtualenv <http://www.virtualenv.org/> >= 15.1.0
- virtualenvwrapper <http://pypi.python.org/pypi/virtualenvwrapper> >= 4.8.2
- git >= 1.7

Getting Started
------------------------

First clone the repository from gitlab and switch to the new directory::

    $ git clone git@gitlab.com:dmitriy.patiuk/html-most-similar-element.git
    $ cd html-most-similar-element

To setup your local environment you should create a virtualenv and install the
necessary requirements:

    # Check that you have python3.7 installed
    $ which python3.7
    $ mkvirtualenv mostsimilar -p `which python3.7`
    (mostsimilar)$ pip install -r requirements.txt

### Run

```
$ python main.py <input_origin_file_path> <input_other_sample_file_path> [target_id]
```
